"use strict";
// const uglify = require('gulp-uglify');

import gulp from "gulp";
import { path } from "./gulp/config/path.js";
// import { copy } from "./gulp/tasks/copy.js";
import { cleanFiles } from "./gulp/tasks/clean.js";
import { html } from "./gulp/tasks/html.js";
import { plugins } from "./gulp/config/plugins.js";
import { imgMin } from "./gulp/tasks/imgmin.js";
import { server } from "./gulp/tasks/server.js";
import { scss } from "./gulp/tasks/scss.js";
import { jsFile } from "./gulp/tasks/js.js";

global.app = {
    path: path,
    gulp: gulp,
    plugins: plugins,
}

function watcher() {
    // gulp.watch(path.watch.files, copy);
    gulp.watch(path.watch.html, html);
    gulp.watch(path.watch.img, imgMin);
    gulp.watch(path.watch.scss, scss);
    gulp.watch(path.watch.js, jsFile);
}

const mainTasks = gulp.parallel(html, imgMin, scss, jsFile);

const dev = gulp.series(cleanFiles, mainTasks, gulp.parallel(watcher, server));
const build = gulp.series(cleanFiles, mainTasks);

export { dev }
export { build }

gulp.task('default', dev);