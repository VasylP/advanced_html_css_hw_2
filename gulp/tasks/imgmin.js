import imageMin from "gulp-imagemin";

export const imgMin = () => {
    return app.gulp.src(app.path.src.img)
        .pipe(imageMin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            interlaced: true,
            optimizationLevel: 3,
        }))
        .pipe(app.gulp.dest(app.path.build.img))
        .pipe(app.plugins.browserSync.stream());
};