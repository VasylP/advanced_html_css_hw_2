import minifyJs from "gulp-minify";
import concat from "gulp-concat";

export const jsFile = () => {
    return app.gulp.src(app.path.src.js, {sourcemaps: true})
        .pipe(concat('index.js'))
        .pipe(app.gulp.dest(`${app.path.src}/scripts/`))
        .pipe(minifyJs())
        .pipe(app.gulp.dest(app.path.build.js))
        .pipe(app.plugins.browserSync.stream())
}