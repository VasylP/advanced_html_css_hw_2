import clean from "gulp-clean";

export const cleanFiles = () => {
    return app.gulp.src(app.path.buildFolder)
        .pipe(clean({force: true}))
}