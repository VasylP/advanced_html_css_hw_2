import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());

const buildFolder = `./dist`;
const srcFolder = `./src`;

export const path = {
    build: {
        js: `${buildFolder}/js/`,
        css: `${buildFolder}/css/`,
        html: `${buildFolder}/`,
        img: `${buildFolder}/img/`,
        // files: `${buildFolder}/files/`,
    },
    src: {
        html: `${srcFolder}/*.html`,
        img: `${srcFolder}/img/*.*`,
        scss: `${srcFolder}/styles/index.scss`,
        js: `${srcFolder}/scripts/*.js`,
        // files: `${srcFolder}/files/**/*.*`,
    },
    watch: {
        html: `${srcFolder}/**/*.html`,
        img: `${srcFolder}/img/**/*.*`,
        scss: `${srcFolder}/styles/**/*.scss`,
        js: `${srcFolder}/scripts/**/*.js`,
        // files: `${srcFolder}/files/**/*.*`,
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder: srcFolder,
    rootFolder: rootFolder,
}